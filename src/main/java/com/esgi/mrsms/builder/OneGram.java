package com.esgi.mrsms.builder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.esgi.mrsms.utils.CleanTextUDF;
import com.esgi.mrsms.utils.CorpusReader;
import com.esgi.mrsms.utils.GlobalSentimentUDF;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class OneGram {

    public void run(String source, SparkSession session, String outputPath) throws Exception {
        // Typer ce qu'on va lire 
        StructField[] structFields = new StructField[]{
            new StructField("col1", DataTypes.StringType, true, Metadata.empty()),
            new StructField("col2", DataTypes.StringType, true, Metadata.empty())
        };
        StructType structType = new StructType(structFields);

        // ajouter les paires de mots dans l'ordre
        List<Row> rows = new ArrayList<>();

        try (CorpusReader reader = new CorpusReader(source)) {
            Iterator<String> iterator = reader.iterator();
            String current = iterator.next();
            String next = iterator.next();
            rows.add(RowFactory.create(current, next));
            while (iterator.hasNext()) {
                current = next;
                next = iterator.next();
                rows.add(RowFactory.create(current, next));
            }
        }


        // register udf
        session.udf().register("love", new GlobalSentimentUDF(session),DataTypes.IntegerType);

        Dataset<Row> allPairs = session.createDataFrame(rows, structType);
        allPairs.createTempView("PAIRS");
        Dataset<Row> countedPairs = allPairs.sqlContext().sql("select col1, col2, count(*) as COUNTER, love(col1, col2,\"\",\"\") as sentiment"
                + " from PAIRS group by col1, col2"
                + " HAVING COUNTER > 100 "
                + " order by COUNTER desc, col1 asc");

        countedPairs.repartition(1).write().option("header", true).option("delimiter", ",").csv(outputPath);
    }
}
