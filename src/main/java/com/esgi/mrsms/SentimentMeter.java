package com.esgi.mrsms;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.io.File;

public class SentimentMeter {

    public void train(String srcPath, SparkSession session, String outputPath) throws AnalysisException {
        assert new File(srcPath).exists();

        StructType metadata = new StructType(new StructField[]{
                new StructField("adjective", DataTypes.StringType, true, Metadata.empty()),
                new StructField("positive_freq", DataTypes.FloatType, true, Metadata.empty()),
                new StructField("negative_freq", DataTypes.FloatType, true, Metadata.empty()),
                new StructField("TomDS", DataTypes.FloatType, true, Metadata.empty()),
        });

        // load data
        Dataset<Row> rows = session.read()
                .option("mode","PERMISSIVE")
                .option("header", true)
                .option("delimiter", ",")
                .schema(metadata)
                .csv(srcPath)
                .select("adjective", "positive_freq", "negative_freq");

        StructType schemaDestination = new StructType(new StructField[]{
                new StructField("word", DataTypes.StringType, true, Metadata.empty()),
                new StructField("sentiment", DataTypes.IntegerType, true, Metadata.empty()),
        });

        Dataset<Row> result = rows.map((Row row ) -> {
            if (row.getFloat(1) >= 0.80) {
                return RowFactory.create(row.getString(0), 1);
            } else if (row.getFloat(2) >= 0.80) {
                return RowFactory.create(row.getString(0), -1);
            }
            return RowFactory.create(row.getString(0), 0);
        } , RowEncoder.apply(schemaDestination));
        result
                .repartition(1)
                .write()
                .option("header", "true")
                .csv(outputPath);
    }
}
